#!/bin/python3 

import os 
import pandas as pd 
import matplotlib.pyplot as plt 

import utils 

def main(): 

    os.system('grep error logfile > error_ux.data')

    df = pd.read_csv('error_ux.data', sep='\s+', names=['t', 'err', 'npt', 'tag'])  
    print(df)
    
    time_scale = 2/utils.Omega/25 
    stime_rotation = utils.Tr / time_scale 
    t = df['t']/stime_rotation 
    err = df['err']

    fig, ax = plt.subplots() 
    ax.plot(t, err*100)
    ax.grid()
    ax.set(xlabel='Time', ylabel='Error (%)') 
    plt.show()

    return 

if __name__ == "__main__": 
    main() 
