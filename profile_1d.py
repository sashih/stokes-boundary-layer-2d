import numpy as np
import matplotlib.pyplot as plt 

import utils 

def main(): 
    path = './' 
    fname = 'box0.f00020' 

    coord, v, stime, nx, ny, nz, nelt = utils.read_f_np(path, fname) 
    x = coord[:,0,:] 
    y = coord[:,1,:] 
    ux = v[:,0,:] 
    uy = v[:,1,:] 
    time_scale = 2/utils.Omega/25 
    stime_rotation = utils.Tr / time_scale 
    t = stime/stime_rotation 

    # Numerical 
    z0, mag, std = utils.z0_profile(ux, y, dz=0.01) 
    # Analytical 
    v1 = -np.exp(z0) * np.sin(z0 + 2*np.pi*t) 

    # Plot 
    fig, ax = plt.subplots() 
    ax.plot(v1, z0, 'k--', label='Analytical') 
    ax.plot(mag, z0, '.-', label='Numerical') 
    ax.legend(loc='lower left') 
    ax.grid() 
    ax.set_ylim([-15, 0]) 
    ax.set(xlabel='Velocity', ylabel='Depth (y)', title='t = %3.2f (t/T)'%t) 
    plt.show()

    return 

if __name__ == "__main__": 
    main() 
