import numpy as np 
import array 
import time 
import struct 
import pandas as pd 
import matplotlib.pylab as plt 
import matplotlib.colors as colors 
import re


U = 0.0043                                  # Velocity scale. Unit = m/s 
Tr = 86164                                  # Sidereal day. Unit = s 
Omega = 2*np.pi/Tr                          # Rotation frequency. Unit = 1/s 
volume = 30*30*20                           # model volumne. Unit = non-diemnsion 
vol_oc = 4*np.pi/3*(3480e3**3-1221.5e3**2)  # Outer core volumn. Unit = m^3 
rho_oc = 1.84e24/vol_oc                     # Outer core densiyt. Unit = kg/m^3 (ref: Stacey) 
area_cmb = 4*np.pi*3480e3**2                # Surface area of CMB 


# [Content] =====================================================================
# 
# cal_par:              nu, L_nu, Omega_, T0 = cal_par(Re)
# read_f:               coord, v, stime, nx, ny, nz, nelt = read_f(path, fname)
# read_f_np:            coord, v, stime, nx, ny, nz, nelt = read_f_np(path, fname) 
# read_vfric:           vfric = read_vfric(path) 
# z0_profile:           z0, mag, std = z0_profile(data, z, dz) 
# plot_profile_2D:      fig, axs = plot_profile_2D(coord, v, intercept, along_axis)
# get_Re:               Re = get_Re(dir1)
# cal_AnalyticOsci:     u, v = cal_AnalyticOsci(z, theta, t)
# 
# ===============================================================================

def cal_par(Re): 
    nu = U**2/(Re**2*Omega)                 # Kinematic viscosity. Unit = m^2/s
    L_nu = np.sqrt(nu/Omega)                # Thickness of Ekman layer. Unit = m 
    Omega_ = Omega*(L_nu/U)                 # Non-dimensional rotation frequency. Unit = na 
    T0 = 2*L_nu/U                             # Time scale. Unit = s 
    return nu, L_nu, Omega_, T0 


def read_f(path, fname): 
    print(path+fname)
    f = open(path+fname, "rb") 
    f.seek(0); print('HD: ', f.read(112))
    print('HD: tag, wdsize, nx, ny, nz, nelt, nelgt, time, iostep, fid, nfileoo, rdcode, p0th, if_press_mesh')
    f.seek(7); nx = int(f.read(2)); 
    f.seek(10); ny = int(f.read(2)); 
    f.seek(13); nz = int(f.read(2)) 
    f.seek(16); nelt = int(f.read(10)) 
    f.seek(38); stime = float(f.read(20)) # simulation time 
    f.seek(132); print('Test value: ', '%6.5f'%struct.unpack('f',f.read(4))) 
    # Data from 112 to 131 is blank. 
    

    if (nz == 1): ndims = 2 
    elif (nz > 1): ndims = 3  
    # print('Number of dimensions: ', ndims) 

    t = time.time() 
    coord = array.array('f')
    f.seek(136+nelt*4) # The offset for coordinates 
    coord.fromfile(f, nelt*ndims*nx*ny*nz)
    coord = np.frombuffer(coord,dtype='float32') 
    coord = coord.reshape((nelt, ndims, nx*ny*nz))
    # print('The shape of coord: ', coord.shape)
    print('Coordinates read. Elapsed time = ', '%3.2e'%(time.time()-t))  

    t = time.time() 
    v = array.array('f')
    f.seek(136+nelt*4+nelt*ndims*nx*ny*nz*4) # The offset for velocity  
    v.fromfile(f, nelt*ndims*nx*ny*nz)
    v = np.frombuffer(v,dtype='float32') 
    v = v.reshape((nelt, ndims, nx*ny*nz))
    # print('The shape of v: ', coord.shape)
    print('Velocity read. Elapsed time = ', '%3.2e'%(time.time()-t))
    f.close() 
    return coord, v, stime, nx, ny, nz, nelt 


def read_f_np(path, fname): 
    # No-print version of read_f 
    f = open(path+fname, "rb") 
    f.seek(7); nx = int(f.read(2)); 
    f.seek(10); ny = int(f.read(2)); 
    f.seek(13); nz = int(f.read(2)) 
    f.seek(16); nelt = int(f.read(10)) 
    f.seek(38); stime = float(f.read(20)) # simulation time 

    if (nz == 1): ndims = 2 
    elif (nz > 1): ndims = 3  
    # print('Number of dimensions: ', ndims) 

    coord = array.array('f')
    f.seek(136+nelt*4) # The offset for coordinates 
    coord.fromfile(f, nelt*ndims*nx*ny*nz)
    coord = np.frombuffer(coord,dtype='float32') 
    coord = coord.reshape((nelt, ndims, nx*ny*nz))

    v = array.array('f')
    f.seek(136+nelt*4+nelt*ndims*nx*ny*nz*4) # The offset for velocity  
    v.fromfile(f, nelt*ndims*nx*ny*nz)
    v = np.frombuffer(v,dtype='float32') 
    v = v.reshape((nelt, ndims, nx*ny*nz))
    f.close() 
    return coord, v, stime, nx, ny, nz, nelt 


def read_vfric(path): 
    vfric = pd.read_csv(path+"\\vfric.data", sep='\s+', header=None, 
                        names=['time', 'v_fric', 'v_fricn', 'v_fric2', 'v_fricn2', 'sx', 'sy', 'sz'])
    return vfric


def z0_profile(data, z, dz=0.05): 

# ================================================
# 'data' and 'z' should have the same structure.  
# For example: 
#    data = v    [:,0,:]    # == vx 
#    z    = coord[:,2,:] 
# ================================================

    L = 20 # the height of my box 
    L = np.max(z)
    L = np.max(abs(z)) 
    N = int(L/dz)
    z0 = -np.arange(0,N)/N * L - dz/2

    mag = np.zeros( (len(z0),) )
    std = np.zeros( (len(z0),) )

    for i in range(len(z0)): 
        idx1 = z >= (z0[i]-dz/2)
        idx2 = z <  (z0[i]+dz/2)
        idx = idx1 & idx2 # element-wise operator. Cannot use 'and' 
        if (np.any(idx)): 
            mag[i] = np.mean( data[idx] )  
            std[i] = np.std(  data[idx] ) 
        else: 
            mag[i] = np.nan 
            std[i] = np.nan
            z0[i]  = np.nan 

    z0 = z0[~np.isnan(z0)]
    mag = mag[~np.isnan(mag)]
    std = std[~np.isnan(std)]

    return z0, mag, std 


def plot_profile_2D(coord, v, intercept, along_axis): 

# Intercept along 'x', 'y', or 'z' -axis 
# 
#   'x'-axis       'y'-axis         'z'-axis 
# 
#    |             |                |
#  z |           z |              y |
#    |             |                |
#    ________      _________        ___________
#        y              x                 x
# 
# X is horizontal, Y is vertical axes on plot 
# Z is the axis for finding intercept 
# 

    dz = 0.05 

    if along_axis=='x': 
        Z = coord[:,0,:]
        X = coord[:,1,:]
        Y = coord[:,2,:]
    elif along_axis=='y': 
        X = coord[:,0,:]
        Z = coord[:,1,:]
        Y = coord[:,2,:]
    elif along_axis=='z': 
        X = coord[:,0,:]
        Y = coord[:,1,:]
        Z = coord[:,2,:]
    else: print('Wrong along_axis! ') 


    Z0 = np.unique(Z)
    if ~(Z0==intercept).any(): 
        tmp = np.abs(Z0-intercept) 
        intercept = Z0[tmp == np.min(tmp)] 
        print('New intercept: %3.2f'%intercept)
    else: print('Intercept: %3.2f'%intercept)

    idx = Z == intercept
    for i in range(1, 10, 1): 
        if np.sum(idx)<2000: # make sure there is enough point inside 
            idx = (Z <= intercept+i*dz) & (Z >= intercept-i*dz)
        elif np.sum(idx)>=2000: 
            range_ = dz*(i-1) 
            print('In the range = [%2.1f, %2.1f]'%(intercept-range_, intercept+range_))
            print('Number of point: %5d'%np.sum(idx))
            break 
    
    fig, axs = plt.subplots(1,3, figsize=[18,4.5])
    j = 0 
    titles=['$u_x$', '$u_y$', '$u_z$']
    for ax in axs: 
        v0 = v[:,j,:] 
        # ax.plot(X[idx], Y[idx], 'k.', ms=0.5)
        CS = ax.tricontourf(X[idx], Y[idx], v0[idx], cmap='RdBu', norm=colors.CenteredNorm(), levels=10)  # 'coolwarm'
        cbar = fig.colorbar(CS, ax=ax) 

        if   along_axis=='x': 
            ax.set(xlabel='y-axis', ylabel='z-axis') 
            ax.set_ylim([-20, 0])
        elif along_axis=='y': 
            ax.set(xlabel='x-axis', ylabel='z-axis') 
            ax.set_ylim([-20, 0])
        elif along_axis=='z': 
            ax.set(xlabel='x-axis', ylabel='y-axis') 

        if j!=0: ax.set(ylabel=''); 

        ax.set(title=titles[j]); 
        j = j+1

    fig.suptitle(r'Intercept at %3.1f $\pm$ %3.2f'%(intercept, range_))
    return fig, axs 

def get_Re(dir1): 
    Re = float(re.search('Re([0-9]*).*', dir1).groups()[0])
    return Re


def cal_AnalyticOsci(z, theta, t): 
    # Calculate the analytical solution for oscillatory Ekman layer 
    #   - z given in unit of viscous depth L_nu, e.g., [-20, 0]. 
    #   - theta given in DEGREE. 
    #   - t given in rotation period, e.g., [0, 5]. 

    theta = theta*np.pi/180
    Omega = 2*np.pi 

    c1 = (np.cos(theta)-1)/2 
    c2 = (np.cos(theta)+1)/2 

    sigma1 = ((1+2*np.cos(theta))*1j)**(1/2)
    sigma2 = ((1-2*np.cos(theta))*1j)**(1/2)

    qx = c1*np.exp(sigma1*z) + c2*np.exp(sigma2*z)  
    qy = c1*np.exp(sigma1*z) - c2*np.exp(sigma2*z)  

    u = (np.exp(1j*Omega*t)*qx).real 
    v = (np.exp(1j*Omega*t-1j*np.pi/2)*qy).real 
    return u, v 


def save_fig(sname): 
    plt.savefig('.\\figures\\'+sname, dpi=300, pad_inches=0.1, bbox_inches='tight') 
    print(sname, ' saved! ') 
    return 