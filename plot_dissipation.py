#!/bin/python 
# Usage       : python plot_dissipation.py 
# 
# Description : Check the energy balance. 
#               Input = work done by boundary friction 
#               Output = change of internal and kinetic energies 
# 
# 
# Created by Sheng-An at 2021.10.01 

import os 
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 

import utils

def main():
    os.system('grep dragx logfile > dragx.data')
    os.system('grep dragy logfile > dragy.data')
    dragx = pd.read_csv('dragx.data', sep='\s+', 
                         names=['step', 'time', 'total', 'pressure', 'viscous', 'component'])
    dragy = pd.read_csv('dragy.data', sep='\s+', 
                         names=['step', 'time', 'total', 'pressure', 'viscous', 'component'])
    dx = - dragx['viscous'] # exerted on the fluid 
    dy = - dragy['viscous']

    ke = pd.read_csv('ke.data', sep='\s+', names=['time', 'disp1', 'disp2', 'ke_dt']) 

    nu, L_nu, Omega_, T0 = utils.cal_par(25) 
    t = ke['time'] * T0/utils.Tr
    u = -np.sin(2*np.pi*t)    # boundary velocity 

    # Numerical solutions 
    work = dx * u             #          (boundary work)
    E_int = ke['disp1']/2     # gradm1   (internal) 
    disp2 = ke['disp2']/2     # comp_sij (internal) 
    E_kin = ke['ke_dt']       # gradm1   (kinetic)
    

    # Analytical solutions 
    omega = 2*np.pi 
    work_analytical =  np.sin(omega*t)**2 + np.sin(omega*t)*np.cos(omega*t) 
    E_int_analytical = 1/2*np.sin(omega*t)**2 + 1/2*np.sin(omega*t)*np.cos(omega*t) + 1/4 
    E_kin_analytical = 1/2*np.sin(omega*t)**2 + 1/2*np.sin(omega*t)*np.cos(omega*t) - 1/4 

    idx = (t>=10) & (t<12) 
    ratio = np.sum(work[idx]) / np.sum(E_int[idx] + E_kin[idx])
    print('Ratio of time-averaged input to output (numerical): %3.2f'%ratio)

    fig, axs = plt.subplots(3,1, figsize=[8,10]) 

    ax = axs[0] 
    ax.plot(t, u, label=r'$u_y$') 
    ax.set(xlabel='Time (t/T)', title='Surface Velocity')

    ax = axs[1] 
    ax.plot(t, dx, color='C0', label=r'$\tau_y$') 
    ax.plot(t, -(np.sin(omega*t)+np.cos(omega*t)), '--', color='C0', label=r'$\tau_y$ (analytical)')
    ax.plot(t, dy, color='C1', label=r'$\tau_z$')
    ax.set(xlabel='Time (t/T)', title='Surface Shear Stress') 

    ax = axs[2]
    ax.plot(t, work  , 'C2', label= 'Input')
    # ax.plot(t, disp2  , 'C1', label='disp2 (comp_sij)')
    ax.plot(t, E_int , 'C3', label=r'$\partial_t E_{int}$') 
    ax.plot(t, E_kin , 'C4', label=r'$\partial_t E_{kin}$') 
    # ax.plot(t, work_analytical, '--', color='C2', label='Input (Analytical)')
    # ax.plot(t, E_kin_analytical, '--', label='ke_dt_analytical')
    # ax.plot(t, E_int_analytical, '--', color='C3', label='Output (Analytical)')
    ax.set(xlabel='Time (t/T)', title='Change of Energy') 


    for ax in axs: 
        ax.grid()
        ax.legend(loc='lower right')
        ax.set_xlim([10,12])
    plt.tight_layout() 
    # plt.show() 
    plt.savefig('fig_foo.jpg')
    
    return 

if __name__ == "__main__": 
    main() 
